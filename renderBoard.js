const _ = require('lodash')
const boardTemplate = require('./templates/board.js')
const boardRowTemplate = require('./templates/boardRow.js')
const boardCellTemplate = require('./templates/boardCell.js')
const wordTemplate = require('./templates/word.js')

module.exports = function(args){
    if(!_.isObject(args)){
        throw new Error('Some arguments are expected')
    }

    if(!(args.$boardTarget instanceof jQuery) || !args.$boardTarget.length){
        throw new Error('Invalid $boardTarget')
    }

    if(!_.isArray(args.grid)){
        throw new Error('Invalid grid')
    }

    var boardFn = _.template(boardTemplate)
    var boardRowFn = _.template(boardRowTemplate)
    var boardCellFn = _.template(boardCellTemplate)

    var $board = $(boardFn({})).appendTo(args.$boardTarget)
    var elementsGrid = [];
    _.forEach(args.grid, function(v, k){
        elementsGrid.push([]);
        var $tr = $(boardRowFn({})).appendTo($board)
        _.forEach(v, function(_v, _k){
            var $td = $(boardCellFn({
                character: _v
            }))
            .appendTo($tr)
            .data({
                coords: {
                    x: _k,
                    y: k
                },
                character: _v
            })
            elementsGrid[k][_k] = {
                character: _v,
                $element: $td
            }
        })
    })

    if(_.isArray(args.boardClasses) && args.boardClasses.length){
        _.forEach(args.boardClasses, function(v, k){
            $board.addClass(v)
        })
    }

    return {
        $board: $board,
        elementsGrid: elementsGrid
    }
}