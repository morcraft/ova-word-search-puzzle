const _ = require('lodash')
const directionBetweenPoints = require('./directionBetweenPoints.js')

module.exports = function(coords){
    var combinations = directionBetweenPoints(coords[0], coords[1]);
    var match = null;
    _.forEach(combinations, function(v, k){
        if(v === true){
            match = k;
            return false;
        }

    })

    return match;
}