const wordsearch = require('wordsearch-generator')
const _ = require('lodash')

module.exports = function(args){
    if(!_.isObject(args)){
        throw new Error('Some arguments are expected.')
    }

    if(!_.isArray(args.words)){
        throw new Error('Words are expected in an array.')
    }

    var grid = wordsearch.createPuzzle(args.puzzleWidth || 20, args.puzzleHeight || 20, args.puzzleLanguage || 'es', args.words);
    return {
        raw: wordsearch.hideWords(grid, 'en'),
        solved: grid
    }
}
