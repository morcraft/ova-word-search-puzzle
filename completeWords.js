const _ = require('lodash')

module.exports = function (args) {
    if(!_.isObject(args)){
        throw new Error('Some arguments are expected.')
    }

    if(!_.isObject(args.instance)){
        throw new Error('A puzzle instance is expected.')
    }

    var word = args.instance.lastSelectedCharacters.join('').toLowerCase();
    var reversedWord = args.instance.lastSelectedCharacters.reverse().join('').toLowerCase();

    if (_.isFunction(args.instance.onWordCompletion)) {
        args.instance.onWordCompletion(word, reversedWord);
    }

    var match = false;
    _.forEach(args.instance.words, function (v, k) {
        var trimmedWord = v.replace(/\s/g, '');
        if (trimmedWord === word) {
            match = v;
        } else {
            if (trimmedWord === reversedWord) {
                match = v;
            }
        }
    })

    if (_.isString(match)) {

        if (!_.isArray(args.instance.solvedWords)) {
            args.instance.solvedWords = []
        }

        if(_.includes(args.instance.solvedWords, match)){
            return true;
        }

        args.instance.solvedWords.push(match)

        if (_.isFunction(args.instance.onWordMatch)) {
            args.instance.onWordMatch(match);
        }
        
        _.forEach(args.instance.$highlightedSquares, function (v, k) {
            v.removeClass('selecting').addClass('matched');
        })

        if (_.size(args.instance.words) === _.size(args.instance.solvedWords)) {
            if (_.isFunction(args.instance.onFinish)) {
                args.instance.onFinish();
            }
        }
    } else {
        if (_.isFunction(args.instance.onFailedWordMatch)) {
            args.instance.onFailedWordMatch(word, reversedWord);
        }
    }
}