const _ = require('lodash')
const wordTemplate = require('./templates/word.js')

module.exports = function(args){
    if(!_.isObject(args)){
        throw new Error('Some arguments are expected')
    }

    if(!(args.$wordsListTarget instanceof jQuery) || !args.$wordsListTarget.length){
        throw new Error('Invalid $wordsListTarget')
    }

    if(!_.isArray(args.words)){
        throw new Error('Invalid words')
    }

    var templateFn = _.template(wordTemplate)

    _.forEach(args.words, function(v, k){
        $(templateFn({
            word: v,
            capitalizedWord: _.capitalize(v)
        })).appendTo(args.$wordsListTarget)
    })
}