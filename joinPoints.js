const _ = require('lodash')
const getDirectionBetweenPoints = require('./getDirectionBetweenPoints.js')
const getConnectingPoints = require('./getConnectingPoints.js')

module.exports = function(args){
    if(!_.isObject(args)){
        throw new Error('Some argumentsa are expected.')
    }

    if(!_.isObject(args.instance)){
        throw new Error('A puzzle instance is expected.')
    }

    if(!_.isObject(args.points)){
        throw new Error('Two points are expected.')
    }

    var direction = getDirectionBetweenPoints(args.points);

    if(direction){

        var connectingPoints = getConnectingPoints(args.points[0], args.points[1])
        _.forEach(args.instance.$highlightedSquares, function(v){
            v.removeClass('selecting');
        })

        args.instance.$highlightedSquares = [];
        args.instance.lastSelectedCharacters = [];

        _.forEach(connectingPoints, function(v, k){
            var registry = args.instance.elementsGrid[v.y][v.x];
            args.instance.lastSelectedCharacters.push(registry.character);
            registry.$element.addClass('selecting');
            args.instance.$highlightedSquares.push(registry.$element)
        });
    }
    else{
        _.forEach(args.instance.$highlightedSquares, function(v){
            v.removeClass('selecting')
        })
    }
}