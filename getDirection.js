const _ = require('lodash')
const directions = require('./directions.js')

module.exports = function(coords){
    var combinations = directions(coords.old.x, coords.old.y);
    var match = null;
    _.forEach(combinations, function(v, k){
        if(_.isEqual(v, coords.new)){
            match = {};
            match[k] = v;
            return false;
        }
    })

    if(_.isObject(match)){
        return Object.keys(match)[0];
    }
}