const _ = require('lodash')

module.exports = function(args){
    if(!_.isObject(args)){
        throw new Error('Some argumentsa are expected.')
    }

    if(!_.isObject(args.instance)){
        throw new Error('A puzzle instance is expected.')
    }

    if(!(args.$element instanceof jQuery) || !args.$element.length){
        throw new Error('Invalid $element');
    }

    args.$element.data('selecting', true).addClass('selecting')
    args.instance.endPoints[0] = args.$element.data('coords');
    args.instance.$board.data('selecting', true)
    args.instance.$highlightedSquares = [];
    args.instance.lastSelectedCharacters = [];
}