const _ = require('lodash')

module.exports = function(c1, c2){
    var _c1 = _.cloneDeep(c1);
    var _c2 = _.cloneDeep(c2);
    var dx = Math.abs(_c2.x - _c1.x),
        dy = Math.abs(_c2.y - _c1.y),
        sx = _c1.x < _c2.x ? 1 : -1,
        sy = _c1.y < _c2.y ? 1 : -1,
        err = dx - dy;

    var points = [{x: _c1.x, y: _c1.y}]

    while (_c1.x != _c2.x || _c1.y != _c2.y) {
        var e2 = 2 * err;
        if (e2 > (dy * -1)) {
            err -= dy;
            _c1.x += sx;
        }
        if (e2 < dx) {
            err += dx;
            _c1.y += sy;
        }

        points.push({x: _c1.x, y: _c1.y})
    }

    return points;
}