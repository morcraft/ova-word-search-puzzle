module.exports = function(x, y){
    return {
        verticalUp: {x: x, y: y-1},
        verticalDown: {x: x, y: y+1},
        horizontalRight: {x: x+1, y: y},
        horizontalLeft: {x: x-1, y: y},
        diagonalRightUp: {x: x+1, y: y-1},
        diagonalRightDown: {x: x+1, y: y+1},
        diagonalLeftUp: {x: x-1, y: y-1},
        diagonalLeftDown: {x: x-1, y: y+1}
    }
}